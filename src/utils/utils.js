export const getBookList = async (log, pass) => {
    
    let requestOptions = {
        method: 'GET',
        'Access-Control-Allow-Origin': '*',
        headers: {
            login: log,
            password: pass
        },
        redirect: 'follow'
    };
    
    let res = null;
    let err = null;

    await fetch("http://localhost:3000/api/books", requestOptions)
      .then(response => response.text())
        .then(result => res = JSON.parse(result))
        .catch(error => err = error);

    return {result: res, error: err};
};

export const getBookById = async (id, log, pass) => {
    
    let requestOptions = {
        method: 'GET',
        'Access-Control-Allow-Origin': '*',
        headers: {
            login: log,
            password: pass
        },
        redirect: 'follow'
    };
    
    let res = null;
    let err = null;

    await fetch(`http://localhost:3000/api/books/${id}`, requestOptions)
      .then(response => response.text())
        .then(result => res = JSON.parse(result))
        .catch(error => err = error);

    return {result: res, error: err};
};

export const createBook = async (book, log, pass) => {
    let raw = JSON.stringify(book);

    let requestOptions = {
        method: 'POST',
        'Access-Control-Allow-Origin': '*',
        headers: {
            login: log,
            password: pass,
            "Content-Type": "application/json"
        },
        body: raw,
        redirect: 'follow'
    };
    
    let res = null;
    let err = null;

    await fetch("http://localhost:3000/api/books", requestOptions)
      .then(response => response.text())
        .then(result => res = JSON.parse(result))
        .catch(error => err = error);

    return {result: res, error: err};
};

export const changeBook = async (id, book, log, pass) => {
    let raw = JSON.stringify(book);

    let requestOptions = {
        method: 'PUT',
        'Access-Control-Allow-Origin': '*',
        headers: {
            login: log,
            password: pass,
            "Content-Type": "application/json"
        },
        body: raw,
        redirect: 'follow'
    };
    
    let res = null;
    let err = null;

    await fetch(`http://localhost:3000/api/books/${id}`, requestOptions)
        .then(response => response.text())
        .then(result => res = JSON.parse(result))
        .catch(error => err = error);

    return {result: res, error: err};
};

export const deleteBook = async (id, log, pass) => {

    let requestOptions = {
        method: 'DELETE',
        'Access-Control-Allow-Origin': '*',
        // mode: 'cors',
        headers: {
            login: log,
            password: pass
        },
        redirect: 'follow'
    };
    
    let res = null;
    let err = null;

    await fetch(`http://localhost:3000/api/books/${id}`, requestOptions)
        .then(response => response.text())
        .then(result => res = result)
        .catch(error => err = error);

  return {result: res, error: err};
};

export const checkUser = async (log, pass) => {
    debugger;
    let result = false;
    await getBookList(log, pass).then(books => {
        if ( !isNaN(books.result.length)) {
            result = true;
        } else {
            result = false;
        }
    });
    return result;
};

export const addUser = async (log, pass) => {
    let raw = JSON.stringify({ "login": log, "password": pass });

    let requestOptions = {
        method: 'POST',
        'Access-Control-Allow-Origin': '*',
        headers: {
            "Content-Type": "application/json"
        },
        body: raw,
        redirect: 'follow'
    };
    
    let res = null;
    let err = null;

    fetch("http://localhost:3000/api/users", requestOptions)
        .then(response => response.text())
        .then(result => res = JSON.parse(result))
        .catch(error => err = error);

    return {result: res, error: err};
};