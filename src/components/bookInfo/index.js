import React, { useState } from 'react';
import MaterialButton from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

import NoPhoto from '../../assets/image/noPhoto.jpg';

import styles from './bookInfo.module.css';

const InfoItem = ({ title, value, onChange, changeMode, type="text" }) => {
	const [_value, setValue] = useState(value);
	const _onChange = e => {
		if (type === 'text') {
			setValue(e.target.value);
		} else {
			setValue(parseInt(e.target.value));
		}
	}
	return (
		<div className={styles.infoItemContainer} >
			{changeMode ? null : <div className={styles.infoItemTitle} >{title}:</div>}
			{changeMode 
			? 
				<TextField label={title} value={_value} onChange={_onChange} onBlur={() => onChange(_value)} variant="outlined" type={type} /> 
			: 
				<div className={styles.infoItemValue} >{value || ""}</div>}
		</div>
	);
};


const BookInfo = ({ book={}, goBack, deleteBook, save, isNew=false  }) => {

	const [isChangeMode, setIsChangeMode] = useState(isNew);
	const [title, setTitle] = useState(book.title || '');
	const [description, setDescription] = useState(book.description || '');
	const [author, setAuthor] = useState(book.author || '');
	const [publisher, setPublisher] = useState(book.publisher || '');
	const [year, setYear] = useState(book.year || '');

	const Book = () => {
		return (
			<div className={styles.bookContainer}>
				<img src={isNew ? NoPhoto : (book.image || NoPhoto)} alt={title} className={styles.bookImage}/>
				<div>
					<InfoItem title='Автор' value={author} onChange={setAuthor} changeMode={isChangeMode} />
					<InfoItem title='Название' value={title} onChange={setTitle} changeMode={isChangeMode} />
					<InfoItem title='Описание' value={description} onChange={setDescription} changeMode={isChangeMode} />
					<InfoItem title='Издательство' value={publisher} onChange={setPublisher} changeMode={isChangeMode} />
					<InfoItem title='Год издания' value={year} onChange={setYear} changeMode={isChangeMode} type="number" />
				</div>
			</div>
		);
	};

	const Btn = ({ title, onClick, className }) => {
		return (
			<div className={className}>
				<MaterialButton variant="contained" onClick={onClick}>
					{title}
				</MaterialButton>
			</div>
		);
	};

	const Btns = () => {
		return (
			<div className={styles.btnsContainer}>
				{isChangeMode ? <>
									<Btn
										title='Сохранить'
										onClick={() => {
											if (!isNew) {
												const newBook = {title, description, author, publisher, year, image: book.image}
												save(newBook);
												setIsChangeMode(false);
											} else {
												const newBook = {
													title, 
													description, 
													author, 
													publisher, 
													year, 
													// image: 'https://sun9-58.userapi.com/Tvd06UqcgOvULZ6mfZYnj3bXXEG39c6kp47MPA/9otGSrUvj_0.jpg'
													image: NoPhoto
												}
												save(newBook);
											}
										}}
										className={styles.btn}
									/>

									<Btn
										title='К списку книг'
										onClick={() => {
											goBack();
										}}
										className={styles.btn}
									/>
								</>
				:
				<>
					<Btn
						title='Изменить'
						onClick={() => {
							setIsChangeMode(true);
						}}
						className={styles.btn}
					/>

					<Btn
						title='Удалить'
						onClick={() => {
							deleteBook();
							goBack();
						}}
						className={styles.btn}
					/>

					<Btn
						title='К списку книг'
						onClick={() => {
							goBack();
						}}
						className={styles.btn}
					/>
				</>
				}
			</div>
		);
	};

	return (
		<div>
			<Book />
			<Btns />
		</div>
	);
};

export default BookInfo;
