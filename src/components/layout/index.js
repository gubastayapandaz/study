import React from 'react';
import { withRouter } from 'react-router-dom';
import MaterialButton from '@material-ui/core/Button';
import styles from './layout.module.css';

const Layout = ({ history, children }) => {
	return (
		<>
			<div className={styles.container} >
				<MaterialButton
					variant="contained"
					onClick={() => {
						history.push('/exit');
					}}
				>
					Выйти
				</MaterialButton>
			</div>
			{children}
		</>
	);
};

export default withRouter(Layout);
