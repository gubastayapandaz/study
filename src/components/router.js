import React from 'react';
import { HashRouter, Switch, Redirect } from 'react-router-dom';
import { useUserContext } from '../context/userContext';

const Router = () => {
	const { routes, defaultPath } = useUserContext();

	return (
		<HashRouter>
			<Switch>
				{routes}
				<Redirect from="/" to={defaultPath} />
			</Switch>
		</HashRouter>
	);
};

export default Router;
