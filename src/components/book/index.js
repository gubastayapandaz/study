import React from 'react';

import NoPhoto from '../../assets/image/noPhoto.jpg';

import styles from './book.module.css';

const Book = ({ book, onClick }) => {
	return (
		<div className={styles.container} onClick={onClick}>
			<img src={book.image} alt={book.title} className={styles.bookImage}/>
			<div>
				<div>{book.author}</div>
				<div>{book.title}</div>
				<div>{book.year}</div>
			</div>
		</div>
	);
};

export default Book;
