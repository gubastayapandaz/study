import { useEffect } from "react";
import { withRouter } from "react-router";
import { useUserContext } from '../context/userContext';

const Exit = ({ history }) => {
	const { logout } = useUserContext();
  useEffect(() => {
	logout();
    history.push("/login");
  });

  return null;
};

export default withRouter(Exit);
