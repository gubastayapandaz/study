import React, { useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import BookInfo from '../../components/bookInfo';
import Layout from '../../components/layout';
import { useUserContext } from '../../context/userContext';
import { getBookById, deleteBook, changeBook } from '../../utils/utils';

const Book = ({ history }) => {
	const id = history.location.search.slice(4);
	const { login, password } = useUserContext();
	const [book, setBook] = useState({});

	useEffect(() => {
		getBookById(id, login, password).then(book => setBook(book.result))
	}, [])

	const goBack = () => {history.push('/')};
	const _deleteBook = () => deleteBook(id, login, password);
	const _saveBook = book => changeBook(id, book, login, password);
	return (
		<Layout>
			{book.id ? <BookInfo book={book} save={_saveBook} deleteBook={_deleteBook} goBack={goBack} /> : null}
		</Layout>
	);
};

export default withRouter(Book);
