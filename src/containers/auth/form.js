import React, { useState } from 'react';
import { withFormik, Field, Form } from 'formik';
import { TextField } from 'formik-material-ui';
import { IconButton} from '@material-ui/core';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import MaterialButton from '@material-ui/core/Button';
import * as Yup from 'yup';
import styles from './auth.module.css';

const AuthSchema = Yup.object().shape({
	login: Yup.string().required('Не заполнено обязательное поле'),
	password: Yup.string().required('Не заполнено обязательное поле')
});


const AuthForm = ({ setErrorMsg, setAlertVisible, values, children, setFieldValue, isSubmitting }) => {
	const [showPassword, setShowPassword] = useState(0);

	return (
		<div className={styles.container}>
		<Form className={styles.form}>
			<h2>Авторизация</h2>
			<h5>Введите логин и пароль для входа в ваш аккаунт с этого устройства</h5>
			<Field
				className={styles.input}
				label="Логин"
				name="login"
				component={TextField}
				type="text"
				variant="outlined"
			/>
			<Field
				className={styles.input}
				label="Пароль"
				name="password"
				variant="outlined"
				type={showPassword ? 'text' : 'password'}
				component={TextField}
				InputProps={{
					endAdornment: (
						<IconButton
							onClick={() => {
								setShowPassword(!showPassword);
							}}
						>
							{showPassword ? <VisibilityOff /> : <Visibility />}
						</IconButton>
					)
				}}
			/>
			<MaterialButton className={styles.submit} disabled={isSubmitting} variant="contained" type="submit">
				Войти
			</MaterialButton>
			<MaterialButton className={styles.submit} disabled={isSubmitting} variant="contained" type="submit" onClick={values.goToRegisterForm}>
				Зарегистрироваться
			</MaterialButton>
		</Form>
		</div>
	);
};

export default (
	withFormik({
		mapPropsToValues: props => {
			return ({
			login: props.login || '',
			password: props.password || '',
			remember: props.remember || false,
			goToRegisterForm: props.goToRegisterForm || null
		})},
		handleSubmit: async (values, { props, setErrors, setSubmitting, resetForm }, c) => {

			props.authorize(values.login, values.password);
		},
		validationSchema: AuthSchema
	})(AuthForm)
);
