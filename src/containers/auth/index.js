import React from 'react';
import { withRouter } from 'react-router-dom';
import { useUserContext } from '../../context/userContext';
import AuthForm from './form';

const AuthScreen = ({ history }) => {
	const { authorize } = useUserContext();

	const goToRegisterForm = () => history.push('/register');

	return (<AuthForm goToRegisterForm={goToRegisterForm} authorize={authorize}/>)
};

export default withRouter(AuthScreen);
