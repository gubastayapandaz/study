import React, { useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import MaterialButton from '@material-ui/core/Button';
import Book from '../../components/book';
import BookInfo from '../../components/bookInfo';
import Layout from '../../components/layout';
import { useUserContext } from '../../context/userContext';
import { getBookList, createBook } from '../../utils/utils';

const Home = ({history}) => {
	const { login, password } = useUserContext();
	const [isCreateNew, setIsCreateNew] = useState(false);
	const [bookList, setBookList] = useState([]);
	const [needUpdate, setNeedUpdate] = useState(false);

	useEffect(() => {
		getBookList(login, password).then(books => setBookList(books.result));
	}, [])

	useEffect(() => {
		if (needUpdate) {
			getBookList(login, password).then(books => setBookList(books.result));
			setNeedUpdate(false);
		}
	}, [needUpdate])

	const onBookClick = id => () => {history.push(`/book?id=${id}`)};
	const goBack = () => {
		setIsCreateNew(false);
		setNeedUpdate(true);
	};
	const saveNew = book => {
		createBook(book, login, password).then(res => goBack());
	};
	return (
		<Layout>
			{isCreateNew ? <BookInfo isNew={true} save={saveNew} goBack={goBack} /> :
			<>
				{bookList.map((book, index) => <Book key={index} book={book} onClick={onBookClick(book.id)} />)}
				<MaterialButton variant="contained" onClick={() => setIsCreateNew(true)}>
						Создать
				</MaterialButton>
			</>
			}
		</Layout>
	);
};

export default withRouter(Home);
