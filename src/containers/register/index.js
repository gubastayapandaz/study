import React from 'react';
import { withRouter } from 'react-router-dom';
import { useUserContext } from '../../context/userContext';
import AuthForm from './form';

const AuthScreen = ({ history }) => {
	const { register } = useUserContext();
	console.log(register)

	const goToAuthForm = () => history.push('/login');

	const _register = (log, pass) => {
		register(log, pass);
		goToAuthForm();
	};


	return (<AuthForm register={_register} goToAuthForm={goToAuthForm}/>)
};

export default withRouter(AuthScreen);
