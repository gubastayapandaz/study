import React, { useState } from 'react';
import { withFormik, Field, Form } from 'formik';
import { TextField } from 'formik-material-ui';
import { IconButton} from '@material-ui/core';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import MaterialButton from '@material-ui/core/Button';
import * as Yup from 'yup';
import styles from './register.module.css';

const AuthSchema = Yup.object().shape({
	login: Yup.string().required('Не заполнено обязательное поле'),
	password: Yup.string().required('Не заполнено обязательное поле')
});


const AuthForm = ({ setErrorMsg, setAlertVisible, values, children, setFieldValue, isSubmitting }) => {
	const [showPassword, setShowPassword] = useState(0);

	return (
		<div className={styles.container}>
		<Form className={styles.form}>
			<h2>Регистрация</h2>
			<h5>Придумайте логин и пароль для входа в ваш аккаунт</h5>
			<Field
				className={styles.input}
				label="Логин"
				name="login"
				component={TextField}
				type="text"
				variant="outlined"
			/>
			<Field
				className={styles.input}
				label="Пароль"
				name="password"
				variant="outlined"
				type={showPassword ? 'text' : 'password'}
				component={TextField}
				InputProps={{
					endAdornment: (
						<IconButton
							onClick={() => {
								setShowPassword(!showPassword);
							}}
						>
							{showPassword ? <VisibilityOff /> : <Visibility />}
						</IconButton>
					)
				}}
			/>
			<MaterialButton className={styles.submit} disabled={isSubmitting} variant="contained" type="submit">
				Зарегистрироваться
			</MaterialButton>
			<MaterialButton className={styles.submit} disabled={isSubmitting} variant="contained" type="submit" onClick={values.goToAuthForm}>
				Вернуться к авторизации
			</MaterialButton>
		</Form>
		</div>
	);
};

export default (
	withFormik({
		mapPropsToValues: props => ({
			login: '',
			password: '',
			goToAuthForm: props.goToAuthForm
		}),
		handleSubmit: async (values, { props, setErrors, setSubmitting, resetForm }) => {
			console.log('props', props)

			props.register(values.login, values.password);
		},
		validationSchema: AuthSchema
	})(AuthForm)
);
