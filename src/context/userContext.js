import React, { useContext, createContext, useEffect, useState } from 'react';
import * as crypto from 'crypto';
import { Route } from 'react-router-dom';
import Register from '../containers/register';
import Home from '../containers/home';
import BookInfo from '../containers/book';
import Auth from '../containers/auth';
import Exit from '../components/exit.js';
import { checkUser, addUser } from '../utils/utils';

const authRoutes = [
	{
		path: '/',
		component: Home,
		exact: true
	},
	{
		path: '/book',
		component: BookInfo,
		exact: false
	},
	{
		path: '/exit',
		component: Exit,
		exact: true
	}
];

const unAuthRoutes = [
	{
		path: '/login',
		component: Auth,
		exact: true
	},
	{
		path: '/register',
		component: Register,
		exact: true
	}
];

const defaultState = {
	isAuthorized: false,
	login: null,
	password: null,
	routes: [],
	defaultPath: '/login'
};

const UserContext = createContext(defaultState);

export const useUserContext = () => useContext(UserContext);

const UserContextProvider = ({ children }) => {
	const [isAuthorized, setIsAuthorized] = useState(defaultState.isAuthorized);
	const [login, setLogin] = useState(defaultState.login);
	const [password, setPassword] = useState(defaultState.password);
	const [routes, setRoutes] = useState(defaultState.routes);
	const [defaultPath, setDefaultPath] = useState(defaultState.defaultPath);

	const getRoutes = () => {
		const _routes = [];
		if (isAuthorized) {
			authRoutes.forEach((el, index) => {
				_routes.push(<Route
					key={index}
					path={el.path}
					exact={el.exact}
					render={() => React.createElement(el.component)}
				/>)
			})
			setDefaultPath('/');
		} else {
			unAuthRoutes.forEach((el, index) => {
				_routes.push(<Route
					key={index}
					path={el.path}
					exact={el.exact}
					render={() => React.createElement(el.component)}
				/>)
			})
			setDefaultPath('/login');
		}
		setRoutes(_routes);
	};

	const register = (log, pass) => {
		addUser(log, pass);
	};

	const authorize = (log, pass) => {
		const hashedPassword = crypto
			.createHash('sha256')
			.update(pass)
			.digest('hex')
		checkUser(log, hashedPassword).then(result => {
			if (result) {
				setLogin(log);
				setPassword(hashedPassword);
				setIsAuthorized(true);
			}
		});
	};

	const logout = () => {
		setLogin(null);
		setPassword(null);
		setIsAuthorized(false);
	};


	useEffect(() => {
		getRoutes();
	// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [isAuthorized]);

	return (
		<UserContext.Provider
			value={{
				isAuthorized,
				setIsAuthorized,
				login,
				setLogin,
				password,
				setPassword,
				routes,
				setRoutes,
				defaultPath,
				setDefaultPath,
				authorize,
				logout,
				register
			}}
		>
			{children}
		</UserContext.Provider>
	);
};

export default UserContextProvider;
